import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode

application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("RDS_DB_NAME", None)
    username = os.environ.get("RDS_USERNAME", None)
    password = os.environ.get("RDS_PASSWORD", None)
    hostname = os.environ.get("RDS_HOSTNAME", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year YEAR(4), title VARCHAR(255), director VARCHAR(255), actor VARCHAR(255), release_date VARCHAR(255), rating DECIMAL(4,2), PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)
try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table")
    create_table()
    print("After create_table")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None

@app.route('/add_movie', methods=['POST'])
def add_movie():
    print("Received request.")
    insert_movie = ("INSERT INTO movies "
                    "(year, title, director, actor, release_date, rating) "
                    "VALUES (%s, %s, %s, %s, %s, %s)")
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    result = ''
    title = request.form['title']
    try:
        cur = cnx.cursor()
        # Check if movie exists 
        movie_details = { 'title' : title }
        check_movie = ("SELECT COUNT(1) FROM movies "
                       "WHERE TITLE=%(title)s")
        cur.execute(check_movie, movie_details)
        if cur.fetchone()[0]:
            result = 'Movie %s could not be inserted - Movie already exists' % title
        else:
            details = (request.form['year'], title, request.form['director'],
                    request.form['actor'], request.form['release_date'], request.form['rating'])
            cur.execute(insert_movie, details)
            cnx.commit()
            result = 'Movie %s successfully inserted' % title  
    except Exception as exp:
        result = 'Movie %s could not be inserted - %s' % (title, exp) 
    return hello(result)

@app.route('/update_movie', methods=['POST'])
def update_movie():
    print("Received request.")
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    result = ''
    title = request.form['title']
    try:
        cur = cnx.cursor()

        # Check if movie exists 
        movie_details = { 'title' : title }
        check_movie = ("SELECT COUNT(1) FROM movies "
                       "WHERE TITLE=%(title)s")
        cur.execute(check_movie, movie_details)

        if cur.fetchone()[0]:
            update_movie = ("UPDATE movies "
                            "SET YEAR=%(year)s, DIRECTOR=%(director)s, ACTOR=%(actor)s, RELEASE_DATE=%(release_date)s, RATING=%(rating)s "
                            "WHERE TITLE=%(title)s")
            movie_details = {
                'year' : request.form['year'], 
                'title' : request.form['title'], 
                'director' : request.form['director'],
                'actor' : request.form['actor'], 
                'release_date' : request.form['release_date'],
                'rating' : request.form['rating'],
            }
            cur.execute(update_movie, movie_details)
            cnx.commit()
            result = 'Movie %s successfully updated' % title  
        else:
            year = str(request.form['year'])
            result = 'Movie with %s and %s does not exist' % (title, year)
    except Exception as exp:
        result = 'Movie %s could not be updated - %s' % (title, exp) 
    return hello(result)

@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    print("Received request.")
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    result = ''
    title = request.form['delete_title']
    try:
        cur = cnx.cursor()
        movie_details = { 'title' : title }

        # Check if movie exists 
        check_movie = ("SELECT COUNT(1) FROM movies "
                       "WHERE TITLE=%(title)s")
        cur.execute(check_movie, movie_details)
        if cur.fetchone()[0]:
            delete_movie = ("DELETE FROM movies "
                            "WHERE TITLE=%(title)s")
            cur.execute(delete_movie, movie_details)
            cnx.commit()
            result = 'Movie %s successfully deleted' % title  
        else:
            result = 'Movie with %s does not exist' % title

    except Exception as exp:
        result = 'Movie %s could not be deleted - %s' % (title, exp) 
    return hello(result)

@app.route('/search_movie', methods=['GET'])
def search_movie():
    print("Received request.")
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    actor = request.args.get('search_actor')
    try:
        cur = cnx.cursor()
        search_movie = ("SELECT title, year, actor FROM movies "
                        "WHERE ACTOR=%(actor)s")
        movie_details = { 'actor' : actor }
        cur.execute(search_movie, movie_details)
        movies = [' - '.join(str(x) for x in movie) for movie in cur.fetchall()]
        result = '\n'.join(movies)
        if result == '':
            result = 'No movies found for actor %s' % actor

    except Exception as exp:
        print(exp)
        result = 'No movies found for actor %s' % actor
    return hello(result)

@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    print("Received request.")
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        get_max_ratings = ("SELECT title, year, actor, director, rating "
                           "FROM movies "
                           "WHERE rating=(SELECT MAX(rating) FROM movies)")
        cur.execute(get_max_ratings)
        movies = [' - '.join(str(x) for x in movie) for movie in cur.fetchall()]
        result = '\n'.join(movies)
    except Exception as exp:
        print(exp)
        result = 'Failed to find highest rating - %s' % exp 
    return hello(result)

@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    print("Received request.")
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    try:
        cur = cnx.cursor()
        get_min_ratings = ("SELECT title, year, actor, director, rating "
                           "FROM movies "
                           "WHERE rating=(SELECT MIN(rating) FROM movies)")
        cur.execute(get_min_ratings)
        movies = [' - '.join(str(x) for x in movie) for movie in cur.fetchall()]
        result = '\n'.join(movies)
    except Exception as exp:
        print(exp)
        result = 'Failed to find highest rating - %s' % exp 
    return hello(result)

@app.route("/")
def hello(result = ''):
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    return render_template('index.html', message=result.split('\n'))

if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
